# Entry

start - https://www.djangoproject.com/start/

django tutorial - https://docs.djangoproject.com/en/1.11/intro/tutorial01/

ask questions - https://www.reddit.com/r/djangolearning/

# Docs

docs - https://docs.djangoproject.com/en/1.11/

quick search docs - http://devdocs.io/django/

class based views docs - http://ccbv.co.uk/

# More verbose tutorial - Try-Django-1.11

Video feed - https://www.youtube.com/watch?v=yDv5FIAeyoY

Github - https://github.com/codingforentrepreneurs/Try-Django-1.11

# Nice django apps

django-rest-framework - http://www.django-rest-framework.org/

django-extensions - https://github.com/django-extensions/django-extensions

django-braces - https://django-braces.readthedocs.io/en/latest/

django-debug-toolbar - https://django-debug-toolbar.readthedocs.io/en/stable/

cookie-cutter - https://github.com/pydanny/cookiecutter-django

social auth for rest-framework - https://github.com/PhilipGarnero/django-rest-framework-social-oauth2

dev-fixtures - https://django-devfixtures.readthedocs.io/en/latest/readme.html#installation


# Books

two scoops of django - version 1.8 (http://kts.org.kw/media/document/2015-05-25-tsd18-final.pdf)

more advanced - https://highperformancedjango.com/

# Nice blog posts

Choices on django models - http://blog.richard.do/index.php/2014/02/how-to-use-enums-for-django-field-choices/

# Daily news

https://www.reddit.com/r/django/

